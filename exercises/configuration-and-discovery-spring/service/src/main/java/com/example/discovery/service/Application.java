package com.example.discovery.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @RestController
    class Greeter {

        private Logger log = LoggerFactory.getLogger(com.example.discovery.service.Application.Greeter.class);

        @Value("${message:Hello default!}")
        private String message;

        @Value("${server.port}")
        private int port;

        @Value("${spring.application.name}")
        private String applicationName;

        @RequestMapping("/greeting")
        ResponseEntity<Map<String, String>> getMessage() {
            log.info("Welcome message: {}", message);
            return ResponseEntity
                    .ok()
                    .header("X-Application-Context", applicationName + ":" + port)
                    .body(Map.of("msg", message));
        }
    }
}
